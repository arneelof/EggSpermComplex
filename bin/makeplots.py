#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

#import jgraph as ig
from pyvis.network import Network
import networkx as nx
import plotly.graph_objects as go


df=pd.read_csv("data/summary.csv")


#df[["dir","foo","Pair","model"]]=df.name.str.split("/",n=3,expand=True)
#df[["id1","id2"]]=df.Pair.str.split("-",n=2,expand=True)
#df[["Type1","foo"]]=df.id1.str.split("_",n=1,expand=True)
#df[["Type2","foo"]]=df.id2.str.split("_",n=1,expand=True)
#df.drop(labels=["name","foo","dir"],axis=1)

#df["homo"]=np.where((df["name1"]==df["name2"]),"Homomer","Heteromer")

#df["type"]=np.where((df["Type1"]=="e")&(df["Type2"]=="e"),"egg-egg",
#                   np.where((df["Type1"]=="s")&(df["Type2"]=="s"),"sperm-sperm"
#                   ,"egg-sperm"))

#maxdf=df.groupby(by="Pair").max()

f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.kdeplot(data=df,x="pTM",hue="type",common_norm=False)
ax.set_xlabel("pTM")
plt.savefig("figures/pTM.png",bbox_inches="tight",dpi=300)
plt.close(fig="all")


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
p=sns.kdeplot(data=df,x="ipTM",hue="type",common_norm=False)
ax.set_xlabel("ipTM")
plt.savefig("figures/ipTM.png",bbox_inches="tight",dpi=300)
plt.close(fig="all")





keys=df["name1"].unique()
keys=np.unique(np.concatenate((keys,df["name2"].unique())))
keys.sort()
len(keys)
f, ax = plt.subplots(figsize=(12., 12.))

newdf=pd.DataFrame({"Protein":[],"ipTM":[]})
temp={}
for d in keys:
    #print (d)
    v=pd.DataFrame(df.loc[((df.name1==d) | (df.name2==d) & (df.name1 != df.name2))]).ipTM.values.tolist()
    #print (v)
    for k in v:
        #print (d,k)
        newdf=newdf.append({"Protein":d,"ipTM":k},ignore_index=True)
        #print (newdf)
    #temp[d]=v
#newdf=pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in temp.items() ]))
    #newdf=pd.DataFrame.from_dict(temp)
#print (newdf)
#print (temp)

f, ax = plt.subplots(figsize=(24., 12.))
my_order = newdf.groupby(by=["Protein"])["ipTM"].median().iloc[::-1].index
sns.set(font_scale=1.0,style="whitegrid",rc={'figure.figsize':(24,12)})
p=sns.violinplot(x="Protein",y="ipTM",data=newdf,order=my_order)
plt.xticks(rotation=80)
plt.savefig("figures/violin-heteromer.png",bbox_inches="tight",dpi=300)
plt.close(fig="all")

#ax.set_title('piTM values per protein')
    #plt.legend(loc = 'lower right')
#df.name1



f, ax = plt.subplots(figsize=(12., 12.))

newdf=pd.DataFrame({"Protein":[],"ipTM":[]})
temp={}
for d in keys:
    #print (d)
    v=pd.DataFrame(df.loc[((df.name1==d) & (df.name2==d))]).ipTM.values.tolist()
    #print (v)
    for k in v:
        #print (d,k)
        newdf=newdf.append({"Protein":d,"ipTM":k},ignore_index=True)
        #print (newdf)
    #temp[d]=v
#newdf=pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in temp.items() ]))
    #newdf=pd.DataFrame.from_dict(temp)
#print (newdf)
#print (temp)

f, ax = plt.subplots(figsize=(24., 12.))
my_order = newdf.groupby(by=["Protein"])["ipTM"].median().iloc[::-1].index
sns.set(font_scale=1.0,style="whitegrid",rc={'figure.figsize':(24,12)})
p=sns.violinplot(x="Protein",y="ipTM",data=newdf,order=my_order)
plt.xticks(rotation=80)
plt.savefig("figures/violin-homomer.png",bbox_inches="tight",dpi=300)
plt.close(fig="all")




f, ax = plt.subplots(figsize=(12., 12.))

newdf=pd.DataFrame({"Protein":[],"ipTM":[]})
temp={}
for d in keys:
    #print (d)
    v=pd.DataFrame(df.loc[(  ((df.name1==d) | (df.name2==d)) & (df.Type1==df.Type2) )]).ipTM.values.tolist()
    #print (v)
    for k in v:
        #print (d,k)
        newdf=newdf.append({"Protein":d,"ipTM":k},ignore_index=True)
        #print (newdf)
    #temp[d]=v
#newdf=pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in temp.items() ]))
    #newdf=pd.DataFrame.from_dict(temp)
#print (newdf)
#print (temp)

f, ax = plt.subplots(figsize=(24., 12.))
my_order = newdf.groupby(by=["Protein"])["ipTM"].median().iloc[::-1].index
sns.set(font_scale=1.0,style="whitegrid",rc={'figure.figsize':(24,12)})
p=sns.violinplot(x="Protein",y="ipTM",data=newdf,order=my_order)
plt.xticks(rotation=80)
plt.savefig("figures/violin-eggsperm.png",bbox_inches="tight",dpi=300)
plt.close(fig="all")



f, ax = plt.subplots(figsize=(12., 12.))

newdf=pd.DataFrame({"Protein":[],"ipTM":[]})
temp={}
for d in keys:
    #print (d)
    v=pd.DataFrame(df.loc[(  ((df.name1==d) | (df.name2==d)) & (df.Type1 != df.Type2) )]).ipTM.values.tolist()
    #print (v)
    for k in v:
        #print (d,k)
        newdf=newdf.append({"Protein":d,"ipTM":k},ignore_index=True)
        #print (newdf)
    #temp[d]=v
#newdf=pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in temp.items() ]))
    #newdf=pd.DataFrame.from_dict(temp)
#print (newdf)
#print (temp)

f, ax = plt.subplots(figsize=(24., 12.))
my_order = newdf.groupby(by=["Protein"])["ipTM"].median().iloc[::-1].index
sns.set(font_scale=1.0,style="whitegrid",rc={'figure.figsize':(24,12)})
p=sns.violinplot(x="Protein",y="ipTM",data=newdf,order=my_order)
plt.xticks(rotation=80)
plt.savefig("figures/violin-same.png",bbox_inches="tight",dpi=300)
plt.close(fig="all")


# Netowrks



cutoff=0.5
topdf=df.loc[df.ipTM>cutoff]

Gstruct=nx.from_pandas_edgelist(topdf,source="name1",target="name2",edge_attr="ipTM")
netStruct=Network(notebook=True,height='1200px', width='100%',)
netStruct.from_nx(Gstruct)


for n in netStruct.nodes:
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    if (n["id"][0]=="s"):
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"     
netStruct.force_atlas_2based()
netStruct.show("figures/network.html")



topdf=df.loc[(df.ipTM>cutoff) & (df.Type1 != df.Type2  ) ]
GstructES=nx.from_pandas_edgelist(topdf,source="name1",target="name2",edge_attr="ipTM")
netStructES=Network(notebook=True,height='1200px', width='100%',)
netStructES.from_nx(GstructES)

